**basics**
*  call me snake
*  he/him/his
*  moonville, OH
*  everything hobbyist

**where to find me**
* [neocities homepage](https://pilssken.neocities.org/); [+profile](https://neocities.org/site/pilssken)
* [mastodon.social](https://mastodon.social/@pilssken) • [artalley.social (nsfw)](https://artalley.social/@pilssken)
* [twitter](https://twitter.com/pilsskens) • [nsfw twitter](https://twitter.com/kinkymccree)
* [instagram](https://www.instagram.com/pilssken/)
*  tumblr - [main](https://pilssken.tumblr.com/) • [art](https://deersnake.tumblr.com/) • [references](https://macheterefs.tumblr.com/)
*  [pillowfort](https://www.pillowfort.social/pilssken)